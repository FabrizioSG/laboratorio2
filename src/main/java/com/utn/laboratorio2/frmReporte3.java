/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utn.laboratorio2;

import com.utn.utilitarios.archivoSecuencial;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author fabri
 */
public class frmReporte3 extends javax.swing.JDialog {

    private Empleado[] empleados;

    /**
     * Creates new form frmReporte3
     */
    public frmReporte3(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
        this.lecturaDatos();
    }

    private String ConvertirdesdeString() {
        Date fecha = this.DCDesde.getDate();
        DateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        String fechaConvertida = formato.format(fecha);
        return fechaConvertida;
    }

    private String ConvertirhastaString() {
        Date fecha = this.DCHasta.getDate();
        DateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        String fechaConvertida = formato.format(fecha);
        return fechaConvertida;
    }
    private DefaultTableModel modelo = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
            return false;
        }
    };
    private String[] cabeceras = {"Nombre", "Fecha de ingreso"};
    private String[][] datos = new String[0][2];

    private void lecturaDatos() {
        archivoSecuencial oArchivo = new archivoSecuencial("src\\main\\java\\Archivos", "Empleados.txt");
        String[] lineas = oArchivo.leerArray();
        this.empleados = new Empleado[lineas.length];
        for (int i = 0; i < lineas.length; i++) {
            String[] fila = lineas[i].split(",");

            String cedula = fila[0];
            String nombre = fila[1];
            String genero = fila[2];
            String departamento = fila[3];
            String fechaNac = fila[4];
            String edad = fila[5];
            String fechaIng = fila[6];
            String ingles = fila[7];
            String puesto = fila[8];
            this.empleados[i] = new Empleado(cedula, nombre, genero, departamento, fechaNac, edad, fechaIng, ingles, puesto);
        }
    }

    private void pintarTabla() {
        this.modelo.setDataVector(datos, cabeceras);
        this.tblEmpleados.setModel(modelo);
    }

    private void redimensionarMatriz() {
        String[][] respaldo = this.datos;

        this.datos = new String[respaldo.length + 1][2];

        for (int f = 0; f < respaldo.length; f++) {
            for (int c = 0; c < respaldo[f].length; c++) {
                this.datos[f][c] = respaldo[f][c];
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        DCDesde = new com.toedter.calendar.JDateChooser();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        DCHasta = new com.toedter.calendar.JDateChooser();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblEmpleados = new javax.swing.JTable();
        btnBuscar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Desde");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Hasta");

        tblEmpleados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblEmpleados);

        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(47, 47, 47)
                .addComponent(jLabel1)
                .addGap(103, 103, 103)
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(DCDesde, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                .addComponent(DCHasta, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(btnBuscar)
                .addGap(23, 23, 23))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(12, 12, 12)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(13, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(DCDesde, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(DCHasta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnBuscar, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap(246, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(64, 64, 64)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(64, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        // TODO add your handling code here:
        
        Date desde = this.DCDesde.getDate();
        Date hasta = this.DCHasta.getDate();
        for (int i = 0; i < this.empleados.length; i++) {
            try {
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                String fecha1 = this.empleados[i].getFecha_ingreso();
                Date fecha = format.parse(fecha1);                
                if (fecha.compareTo(desde) > 0 && fecha.compareTo(hasta) < 0) {
                    this.redimensionarMatriz();
                    this.datos[this.datos.length - 1][0] = this.empleados[i].getNombre();
                    this.datos[this.datos.length - 1][1] = this.empleados[i].getFecha_ingreso();
                    this.pintarTabla();
                }
            } catch (ParseException ex) {
                Logger.getLogger(frmReporte3.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }//GEN-LAST:event_btnBuscarActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.toedter.calendar.JDateChooser DCDesde;
    private com.toedter.calendar.JDateChooser DCHasta;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblEmpleados;
    // End of variables declaration//GEN-END:variables
}
